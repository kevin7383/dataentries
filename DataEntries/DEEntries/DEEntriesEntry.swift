//
//	DEEntriesEntry.swift
//
//	Create by Gabani M1 on 7/12/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DEEntriesEntry : NSObject, NSCoding{

	var aPI : String!
	var auth : String!
	var category : String!
	var cors : String!
	var descriptionField : String!
	var hTTPS : Bool!
	var link : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		aPI = dictionary["API"] as? String == nil ? "" : dictionary["API"] as? String
		auth = dictionary["Auth"] as? String == nil ? "" : dictionary["Auth"] as? String
		category = dictionary["Category"] as? String == nil ? "" : dictionary["Category"] as? String
		cors = dictionary["Cors"] as? String == nil ? "" : dictionary["Cors"] as? String
		descriptionField = dictionary["Description"] as? String == nil ? "" : dictionary["Description"] as? String
		hTTPS = dictionary["HTTPS"] as? Bool == nil ? false : dictionary["HTTPS"] as? Bool
		link = dictionary["Link"] as? String == nil ? "" : dictionary["Link"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if aPI != nil{
			dictionary["API"] = aPI
		}
		if auth != nil{
			dictionary["Auth"] = auth
		}
		if category != nil{
			dictionary["Category"] = category
		}
		if cors != nil{
			dictionary["Cors"] = cors
		}
		if descriptionField != nil{
			dictionary["Description"] = descriptionField
		}
		if hTTPS != nil{
			dictionary["HTTPS"] = hTTPS
		}
		if link != nil{
			dictionary["Link"] = link
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         aPI = aDecoder.decodeObject(forKey: "API") as? String
         auth = aDecoder.decodeObject(forKey: "Auth") as? String
         category = aDecoder.decodeObject(forKey: "Category") as? String
         cors = aDecoder.decodeObject(forKey: "Cors") as? String
         descriptionField = aDecoder.decodeObject(forKey: "Description") as? String
         hTTPS = aDecoder.decodeObject(forKey: "HTTPS") as? Bool
         link = aDecoder.decodeObject(forKey: "Link") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if aPI != nil{
			aCoder.encode(aPI, forKey: "API")
		}
		if auth != nil{
			aCoder.encode(auth, forKey: "Auth")
		}
		if category != nil{
			aCoder.encode(category, forKey: "Category")
		}
		if cors != nil{
			aCoder.encode(cors, forKey: "Cors")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "Description")
		}
		if hTTPS != nil{
			aCoder.encode(hTTPS, forKey: "HTTPS")
		}
		if link != nil{
			aCoder.encode(link, forKey: "Link")
		}

	}

}