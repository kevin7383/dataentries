//
//	DEEntries.swift
//
//	Create by Gabani M1 on 7/12/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DEEntries : NSObject, NSCoding{
    
	var count : Int!
	var entries : [DEEntriesEntry]!

	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		count = dictionary["count"] as? Int == nil ? 0 : dictionary["count"] as? Int
		entries = [DEEntriesEntry]()
		if let entriesArray = dictionary["entries"] as? [NSDictionary]{
			for dic in entriesArray{
				let value = DEEntriesEntry(fromDictionary: dic)
				entries.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if count != nil{
			dictionary["count"] = count
		}
		if entries != nil{
			var dictionaryElements = [NSDictionary]()
			for entriesElement in entries {
				dictionaryElements.append(entriesElement.toDictionary())
			}
			dictionary["entries"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         count = aDecoder.decodeObject(forKey: "count") as? Int
         entries = aDecoder.decodeObject(forKey: "entries") as? [DEEntriesEntry]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if count != nil{
			aCoder.encode(count, forKey: "count")
		}
		if entries != nil{
			aCoder.encode(entries, forKey: "entries")
		}

	}

}
