//
//  ViewController.swift
//  DataEntries
//
//  Created by Gabani M1 on 07/12/21.
//

import UIKit
import Alamofire
import SDWebImage

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var myImageView: UIImageView!
    
    //MARK: Variables
    var arrMainData: [DEEntriesEntry] = [DEEntriesEntry]()
    
    //MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myImageView.layer.cornerRadius = myImageView.frame.height/2
        
        self.tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        getAPI()
        getImageAPI()
    }
    
    //MARK: TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMainData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        
        let dicData = arrMainData[indexPath.row]
        
        let name = dicData.aPI ?? ""
        let des = dicData.descriptionField ?? ""
        let ctg = dicData.category ?? ""
        let link = dicData.link ?? ""
        
        cell.lblName.text = name
        cell.lblDesc.text = des
        cell.lblCate.text = ctg
        cell.lblLink.text = link
        
        cell.lblLink.tag = indexPath.row
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onClicLabel(sender:)))
        cell.lblLink.isUserInteractionEnabled = true
        cell.lblLink.addGestureRecognizer(tap)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    @objc func onClicLabel(sender:UITapGestureRecognizer)
    {
        let dicData = arrMainData[sender.view?.tag ?? 0]
        
        let link = dicData.link ?? ""
        
        openUrl(urlString: link)
    }
    
    func openUrl(urlString:String!) {
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    //MARK: API Calling
    
    func getAPI(){
        
        APIClient.sharedInstance.showIndicator()
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(ENTRIES, parameters: [:]) { response, error, statusCode in
            
            print("statusCode \(String(describing: statusCode))")
            print("response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    if let responseData = response
                    {
                        let arrEntires = responseData.value(forKey: "entries") as? NSArray
                        
                        for objData in arrEntires!{
                            let dicData = DEEntriesEntry(fromDictionary: objData as! NSDictionary)
                            self.arrMainData.append(dicData)
                        }
                        self.tableView.reloadData()
                    }
                }
            }else{
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        }
    }
    func getImageAPI(){
    
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(BREEDS_IMAGES, parameters: [:]) { response, error, statusCode in
            
            print("statusCode \(String(describing: statusCode))")
            print("response \(String(describing: response))")
            
            if error == nil{
                
                if statusCode == 200{
                    
                    if let responseData = response{
                        
                        var image = responseData.value(forKey: "message") as? String
                        image = image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                        let url = URL(string: "\(image!)")
                        self.myImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                        self.myImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "LOGO"))
                    }
                }
                
            }else{
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: "message", message: "", cancelButtonTitle: "OK")
            }
            
        }
    }
}

