//
//  AppDelegate.swift
//  DataEntries
//
//  Created by Gabani M1 on 07/12/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var dicLoginUserDetails: DEEntriesEntry?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
    
    func saveCurrentUserData(dic: DEEntriesEntry)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: dic)
        UserDefaults.standard.setValue(data, forKey: "currentUserData")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentUserData() -> DEEntriesEntry
    {
        if let data = UserDefaults.standard.object(forKey: "currentUserData"){
            
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! DEEntriesEntry
        }
        return DEEntriesEntry.init()
    }


}

